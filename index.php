<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>COINMA</title>

	<style type="text/css">
		.cuerpo{
			font-family: sans-serif;
			background-color: #DEDEDE;
			margin: auto;
			width: 80%;
			max-width: 900px;
			padding: 30px;
			border: 1px solid rgba(0,0,0,0.2);
		}
		.primero{
			background-color: white;
		}
		.segundo{
		}
		.cuarto{
		}
		#nombre{
		  width: 100%;
		  height: 30px;
		}
		#email{
		  width: 100%;
		  height: 30px;
		  margin-top: 5px;
		}

		#textarea{
		  width: 99%;
		  margin-right: 40px;
		  margin-top: 10px;
		}

		#button{
		  width: 100%;
		  height: 36px;
		  background: #fff;
		}
		.contacto{
		  font-family: sans-serif;
		  width: 800px;
		  height: auto;
		  background: #7a7a7a;
		  padding: 10px;
		}
		textarea {
			font-family: sans-serif;
		  resize: none;
		}
	</style>

</head>
<body class="cuerpo">
	<!–- ------------------------------------------------------------------------------------------------------------------------------- -–>
	<form class="primero">
		<br><br>
		<center><img src="logo.png" alt="some_text" style="width:450px;height: px;">
		<h2>Construcciones, instalaciones y mantenimientos</h2></center><br>
	</form>
	<!–- ------------------------------------------------------------------------------------------------------------------------------- -–>
	<form class="segundo">
		<center><h2>Historia</h2></center>
		<DIV ALIGN="justify"><p>En el año 2010 se creó coinma (Construcciones, Instalaciones y Mantenimientos), la cual fue dirigida al
		   mercado del ramo de la construcción residencial, y de las telecomunicaciones durante un año y medio se
		   trabajó solo en la ciudad de Zamora Michoacán y Chilpacingo Guerrero, un año después se tuvo la
		   oportunidad de ingresar al ramo industrial y comercial, y con base a la promoción personalizada fue como la
		   inmobiliaria All In One y Fabrica Coronado nos abrieron la puertas. Con base a la experiencia de los
		   integrantes, en el área de las Telecomunicaciones, fue que en Mayo de 2014, SICE nos da la oportunidad de
		   ser proveedores, atendiendo solo emergencias y correctivos, para posteriormente ofrecernos la oportunidad de
		   atender la zona de Zamora y zona Ocotlá.</p></DIV>
	</form>
	<!–- ------------------------------------------------------------------------------------------------------------------------------- -–>
	<form class="tercero">
		<center><h2>Experiencia</h2></center>
		<DIV ALIGN="justify"><h3>GTECH SERVICIOS DE MÉXICO</h3><p>(Pronósticos para la asistencia pública).Marzo 2012 a la Actualidad
		   Contrato de iguala para atender servicios de mantenimientos preventivos y emergencias.<br> -Mantenimiento a equipos electrónicos tales como monitor, impresor, lector, modem, antena VSAT e instalación de la misma y suministro de papelería publicitaría, en gran parte de la Zona de Zamora  Michoacán, Chilpancingo de los Bravo Guerrero y parte de Guanajuato.</p></DIV>
	</form>
	<!–- ------------------------------------------------------------------------------------------------------------------------------- -–>
	<form class="cuarto">
		<DIV ALIGN="justify"><h3>MISSION DE MEXICO</h3><p>(Empacadora en Uruapan Michoacán)2015 a la Actualidad<br>- Mantenimiento de Edificios, de Equipos de Aire Acondicionado, Pintura e Impermeabilizado de Edificios.</p></DIV>
	</form>
	<!–- ------------------------------------------------------------------------------------------------------------------------------- -–>
	<form class="quinto">
		<DIV align="justify"><h3>LALA</h3> <p>(Centro de Distribución Michoacán, Jalisco y Guanajuato) Mantenimiento de Edificios, Trabajos por
		Proyecto. De Septiembre de 2014 a la Actualidad.<br>- Mantenimiento de Edificios, de Equipos de Aire Acondicionado, Pintura e Impermeabilizado de Edificios, rotulados, herrería, obra civil.</p></DIV>
	</form>
	<!–- ------------------------------------------------------------------------------------------------------------------------------- -–>
	<form class="sexo">
		<DIV align="justify"><h3>SICE (SISTEMAS INTEGRALES DE COMUNICACIÓN Y ENERGÍA)</h3><p>Proveedor de servicios de Huawei y AT&T. De Mayo de 2014 a la Actualidad. Contrato de iguala para atender servicios de mantenimientos preventivos y emergencias, De Julio de 2014 a la Actualidad.<br>- Instalación, desinstalación y Mantenimientos de enlaces de MW y RF, emergencias y mantenimientos de Fallos de Energía, Plantas de Emergencia, Aires Acondicionados, sistemas de Tierra, Limpieza de Predios para los sitios de Telefónica y AT&T en la región, herrería, pintura, impermeabilizados, obra civil, la cual abarca las ciudades de Zamora, La Piedad, Sahuayo, Jiquilpan, Los Reyes, La Barca. Pénjamo, Panindícuaro, Zacapu y Ocotlán </p></DIV>
	</form>
	<!–- ------------------------------------------------------------------------------------------------------------------------------- -–>
	<form class="sexo">
		<DIV align="justify"><h3>SERTRES DEL NORTE</h3><p>Mantenimientos Correctivos. De Febrero de 2014 a la Actualidad
		<br>- Mantenimiento Correctivos de Equipos de Refrigeración (A/A).
		Fabrica Coronado. Mantenimientos e Instalaciones, de Septiembre de 2013 a la Actualidad.
		<br>- Mantenimiento e Instalación de Redes Eléctricas y Pintura.</p></DIV>
	</form>
	<!–- ------------------------------------------------------------------------------------------------------------------------------- -–>
	<form class="sexo">
		<DIV align="justify"><h3>Inmobiliaria All in One Service, Mantenimiento de Edificios e Instalaciones Publicitarias. De Abril de 2013 a la Actualidad.</h3><p><br>- Mantenimiento de Instalaciones Eléctricas, Fontanería, Mantenimiento de Instalaciones, Hidráulicas, Mantenimiento de Equipos de Refrigeración, Construcción de Estructuras para Anuncios Publicitarios, Instalaciones Eléctricas de Fraccionamientos.</p></DIV>
	</form>
	<!–- ------------------------------------------------------------------------------------------------------------------------------- -–>
	<form class="sexo">
		<DIV align="justify"><h3>Caja Popular Mexicana</h3>Mantenimientos de AA de Zona Zamora<br>– Jalisco <br>– Guanajuato, de Enero de 2016 a la actualidad</DIV>
	</form>
	<!–- ------------------------------------------------------------------------------------------------------------------------------- -–>
	<form class="sexo">
		<DIV align="justify"><h3>Caja Popular Santuario Guadalupano (ALIANZA)</h3><p>Mantenimientos de AA Zona Zamora de Septiembre 
de 2015 a la actualidad<br><br><br><br>Si desea ponerse en contacto con nosotros llámenos o envíenos un correo</p></DIV>
	</form>
	<!–- ------------------------------------------------------------------------------------------------------------------------------- -–>
	<form class="ultimo">
		<DIV ALIGN="justify"><h2>Dirección</h2><p>COINMA( Construciones, Instalaciones y Mantenimientos)
		<br>Calle Sevilla Nte. No. 19
		<br>Col. Monte Olivo
		<br>Zamora, Mich.
		<br>Tel. Cell. 0443511120023</p></DIV>
	</form>
	<!–- ------------------------------------------------------------------------------------------------------------------------------- -–>
	<h2>Contactanos</h2>
	<form action="contacto.php" method="post" class="contacto">
	    <input type="text" placeholder="Nombre" id="nombre" name="nombre">
	    <input type="email" placeholder="Ingresar email" id="email" name="email">
	    <textarea name="mensaje" rows="8" cols="47" id="textarea" placeholder="Ingresar Mensaje aqui"></textarea>
	    <input type="submit" name="button" value="Enviar Mensaje" id="button">
	 </form>
</body>
</html>